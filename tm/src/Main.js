import MainColumn from "./MainComponents/MainColumn";
import SideColumn from "./MainComponents/SideColumn";
const Main = ()=>{
    return (
        <section id="main-content">
            {/* <h2>Main component</h2>   */}
            <MainColumn />
            <SideColumn />
        </section>
        
    );
}

export default Main;