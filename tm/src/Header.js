import MainNavBar from "./Navigation/MainNavBar"

const Header = () => {
    return ( 
        <header>
            <h2>Task manager</h2>
            <MainNavBar />
        </header>
     );
}
 
export default Header;