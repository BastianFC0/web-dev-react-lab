import { useState } from "react";
import TaskStats from "../TaskItemComponents/TaskStats"

const SideColumn =  ()=>{

    let [name, setName] = useState('init');
    //let name = 'peter';

    let [view, setView] = useState(0);

    function handleBtnClick(){
        if(name ==='peter'){
            {setName("lim");}
        }else{
            {setName("peter");}
        }
        console.log('name', name);
    }

    function viewUp(){
        {setView(++view);}
    }
    
    return(
        <section id="side-column">
            <h3>Stats conent</h3>
            <TaskStats />
            <button onClick={handleBtnClick}>change name</button>
            <button onClick={viewUp}>view increment</button>
            <p>Views: {view} </p>
            <p>nom: {name}</p>
        </section>
        
    );
}
export default SideColumn;