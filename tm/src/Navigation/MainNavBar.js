const MainNavBar = () => {
    return ( 
        <nav className="main-menu">
            <ul>
                <li><a href="#Home"> Home</a></li>
                <li><a href="#"> List Tasks</a></li>
                <li><a href="#"> New Task</a></li>
                <li><a href="#"> Contact</a></li>
            </ul>
        </nav>
     );
}
export default MainNavBar;