const SideColumn = () => {
    return(
        <section id="section_SideColumn">
            <h3>Stats</h3>
            <TaskStats/>
        </section>
    );
}

export default SideColumn;