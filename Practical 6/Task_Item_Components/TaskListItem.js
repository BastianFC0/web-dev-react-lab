const TaskListItem = () => {
    let taskList = [
        {TaskID:1,title:" What the Oxford English Dictionary Doesn\\'t Tell You About Coding ",
        resp:"nasr", nberHrs:13,desc:"content of  content ofcontent of10 Misconceptions Your Boss Has About Coding ",
        status:"ongoing"},
        {"TaskID":2,"title":" 10 Best Facebook Pages of All Time About Coding ","resp":"nasr", "nberHrs":11,"desc":"content of  content ofcontent of What the Oxford English Dictionary Doesn\\'t Tell You About Coding ","status":"cancelled"},
        {"TaskID":3,"title":" 10 Inspirational Graphics About Coding ","resp":"badr", "nberHrs":24,"desc":"content of  content ofcontent of 10 Best Facebook Pages of All Time About Coding ","status":"done"},
        {"TaskID":4,"title":" 10 Sites to Help You Become an Expert in Coding ","resp":"yacine", "nberHrs":24,"desc":"content of  content ofcontent of 10 Inspirational Graphics About Coding ","status":"cancelled"},
        {"TaskID":5,"title":"The Next Big Thing in Coding ","resp":"imene", "nberHrs":21,"desc":"content of  content ofcontent of 10 Sites to Help You Become an Expert in Coding ","status":"ongoing"},
        {"TaskID":6,"title":"15 Best Twitter Accounts to Learn About Coding ","resp":"imene", "nberHrs":14,"desc":"content of  content ofcontent ofThe Next Big Thing in Coding ","status":"ongoing"} 
    ];

    return(
        <section id="section_ListItems">
            {taskList.map(
                (task) => (
            <section className="section_ListItem">
                <section className="section_RowItem">
                    <span>id:1</span>
                    <span>title: Wow</span>
                </section>
                <section className="section_RowItem">
                    <span>owner: Man</span>
                    <span>status: ongoing</span>
                </section>
            </section>
        
                )
            )}
        </section>
            
    );
}

export default TaskListItem;