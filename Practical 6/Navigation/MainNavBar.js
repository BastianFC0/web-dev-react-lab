const MaimNavBar = () => {
    return(
        <section className="section_Nav">
            <section className="section_NavLinks">
                <a href="" className="a_Href">Home</a>
            </section>
            <a href="" className="a_Href">List Tasks</a>
            <a href="" className="a_Href">New Task</a>
            <a href="" className="a_Href">Contact</a>
        </section>
    );
}

export default MainNavBar;